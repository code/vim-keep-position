function! keep_position#(command) abort
  let view = winsaveview()
  execute a:command
  call winrestview(view)
endfunction
