keep\_position.vim
==================

This tiny plugin provides a command prefix to run commands without moving the
cursor or window view around.  It does this by saving the window view before
running the command, and then restoring it afterwards.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
