"
" keep_position.vim: User command wrapper to preserve cursor position and
" buffer view.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_keep_position') || &compatible || v:version < 700
  finish
endif
let loaded_keep_position = 1

" User command definition refers to autoloaded function
command! -bar -complete=command -nargs=+ KeepPosition
      \ call keep_position#(<q-args>)
